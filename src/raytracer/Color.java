package raytracer;

public class Color extends Tuple {
    public Color(float r, float g, float b){
        super(r, g, b, 0);
    }

    /**
     * Returns the red value of this Color, stored in the x variable of the Tuple
     */
    public float red() {
        return this.getX();
    }

    /**
     * Returns the green value of this Color, stored in the y variable of the Tuple
     */
    public float green() {
        return this.getY();
    }

    /**
     * Returns the blue value of this Color, stored in the z variable of the Tuple
     */
    public float blue() {
        return this.getZ();
    }

    public Color multiply(Color color2) {
        float x = this.red() * color2.red();
        float y = this.green() * color2.green();
        float z = this.blue() * color2.blue();

        return new Color(x, y, z);
    }
}

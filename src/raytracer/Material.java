package raytracer;

public class Material {
    private Color _color;
    private float _ambient;
    private float _diffuse;
    private float _specular;
    private float _shininess;

    public Material(){
        _color = new Color(1, 1, 1);
        _ambient = 0.1f;
        _diffuse = 0.9f;
        _specular = 0.9f;
        _shininess = 200;
    }

    public Color addLighting(PointLight light, Tuple point, Tuple eyev, Tuple normalv) {
        // combine material and light color
        Color newColor = _color.multiply(light.getIntensity());

        // find direction to light source
        Tuple lightDirection = light.getPosition().subtract(point).normalize();

        // compute ambient
        Tuple ambient = newColor.scale(_ambient);
        Tuple diffuse = new Color(0, 0, 0);
        Tuple specular = new Color(0, 0, 0);

        float lightAngle = lightDirection.dotProduct(normalv);
        if (lightAngle < 0){
            // set diffuse and specular to black
            diffuse = new Color(0, 0, 0);
            specular = new Color(0, 0, 0);
        } else {
            // calculate diffuse
            diffuse = newColor.scale(_diffuse).scale(lightAngle);

            Tuple reflectDirection = lightDirection.reflect(normalv).scale(-1);
            float reflectAngle = reflectDirection.dotProduct(eyev);
            if (reflectAngle <= 0){
                // set specular to black
                specular = new Color(0, 0, 0);
            } else {
                // calculate specular
                float factor = (float)Math.pow(reflectAngle, _shininess);
                specular = light.getIntensity().scale(_specular).scale(factor);
            }
        }

        // add together colors and return them
        Tuple returnColor = ambient.add(diffuse).add(specular);
        return new Color(returnColor.getX(), returnColor.getY(), returnColor.getZ());
    }

    public Color getColor() { return _color; }
    public float getAmbient() { return _ambient; }
    public float getDiffuse() { return _diffuse; }
    public float getSpecular() { return _specular; }
    public float getShininess() { return _shininess; }

    public void setColor(Color color) { _color = color; }
    public void setAmbient(float ambient) { _ambient = ambient; }
    public void setDiffuse(float diffuse) { _diffuse = diffuse; }
    public void setSpecular(float specular) { _specular = specular; }
    public void setShininess(float shininess) { _shininess = shininess; }
}

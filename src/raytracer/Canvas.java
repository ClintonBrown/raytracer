package raytracer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class Canvas {
    private final String PPM_IDENTIFIER = "P3";
    private final int PPM_COLOR_MAX = 255;
    private final int PPM_LINE_LENGTH = 70;
    private int _width;
    private int _height;
    private Pixel[][] _pixelGrid;

    public Canvas(int width, int height) {
        _width = width;
        _height = height;
        initializePixelGrid();
    }

    private void initializePixelGrid(){
        _pixelGrid = new Pixel[this.getHeight()][this.getWidth()];
        for (int r = 0; r < this.getHeight(); r++){
            for (int c = 0; c < this.getWidth(); c++){
                this.setPixel(c, r, new Pixel());
            }
        }
    }

    public void writeToPPM(String fileName) {
        Writer writer = null;
        try {
            String ppmText = createPPMString();
            writer = new FileWriter(new File(fileName).getAbsoluteFile());
            writer.write(ppmText);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (writer != null){
                    writer.flush();
                    writer.close();
                }
            } catch (IOException e){
                e.printStackTrace();
            }
        }
    }

    String createPPMString(){
        String header = buildPPMHeader().concat("\n");
        String pixelData = buildPPMPixelData();

        return header.concat(pixelData).concat("\n");
    }

    String buildPPMHeader(){
        StringBuilder header = new StringBuilder();
        final String PPM_WIDTH_HEIGHT = this.getWidth() + " " + this.getHeight();
        header.append(PPM_IDENTIFIER);
        header.append("\n");
        header.append(PPM_WIDTH_HEIGHT);
        header.append("\n");
        header.append(PPM_COLOR_MAX);

        return header.toString();
    }

    String buildPPMPixelData(){
        StringBuilder pixels = new StringBuilder();
        StringBuilder line = new StringBuilder();
        for (Pixel[] pixelLine : _pixelGrid){
            for (Pixel pixel : pixelLine){
                Color pixelColor = pixel.getColor();
                int red = clampPixelValue(pixelColor.red());
                int green = clampPixelValue(pixelColor.green());
                int blue = clampPixelValue(pixelColor.blue());
                appendToPPMColorLine(pixels, line, String.valueOf(red));
                appendToPPMColorLine(pixels, line, " ");
                appendToPPMColorLine(pixels, line, String.valueOf(green));
                appendToPPMColorLine(pixels, line, " ");
                appendToPPMColorLine(pixels, line, String.valueOf(blue));
                appendToPPMColorLine(pixels, line, " ");
            }
            pixels.append(line.toString().trim());
            pixels.append("\n");
            line.setLength(0);
        }
        return pixels.toString().trim();
    }

    private int clampPixelValue(float pixelColorValue){
        int pixelValue = Math.round(pixelColorValue*PPM_COLOR_MAX);
        if (pixelValue > PPM_COLOR_MAX){
            pixelValue = PPM_COLOR_MAX;
        } else if (pixelValue < 0){
            pixelValue = 0;
        }
        return pixelValue;
    }

    private void appendToPPMColorLine(StringBuilder pixels, StringBuilder line, String value){
        if (line.length() + value.length() < PPM_LINE_LENGTH) {
            line.append(value);
        } else {
            pixels.append(line.toString().trim());
            pixels.append("\n");
            line.setLength(0);
            line.append(value);
        }
    }

    int getWidth() { return _width; }
    public int getHeight() { return _height; }
    Pixel[][] getPixelGrid() {
        return _pixelGrid;
    }
    Pixel getPixel(int x, int y) { return _pixelGrid[y][x]; }

    public void setPixel(int x, int y, Pixel pixel){
        _pixelGrid[y][x] = pixel;
    }
}

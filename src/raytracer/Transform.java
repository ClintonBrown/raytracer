package raytracer;

import java.util.List;

/**
 * Class that creates intermediate transformation objects for performing matrix operations.
 */
public class Transform {
    private final float PI = (float)Math.PI;

    /**
     * Creates a Matrix for translating (moving) a point.
     */
    public static Matrix translation(float x, float y, float z) {
        Matrix identity = Matrix.createIdentity();
        List<List<Float>> toTranslate = identity.getMatrix();
        float[] pointValues = { x, y, z };
        for (int row = 0; row < toTranslate.size()-1; row++){
            toTranslate.get(row).set(toTranslate.get(row).size()-1, pointValues[row]);
        }
        return identity;
    }

    /**
     * Creates a Matrix for scaling the size of a point or vector.
     */
    public static Matrix scaling(float x, float y, float z) {
        Matrix identity = Matrix.createIdentity();
        List<List<Float>> toScale = identity.getMatrix();
        float[] pointValues = { x, y, z };
        for (int row = 0; row < toScale.size()-1; row++){
            for (int col = 0; col < toScale.get(row).size(); col++){
                if (row == col){
                    toScale.get(row).set(col, pointValues[row]);
                }
            }
        }
        return identity;
    }

    /**
     * Creates a Matrix for rotating points about the X axis.<br/>
     * <strong>Made to work with 4x4 matrix explicitly.</strong>
     */
    public static Matrix rotateX(float radians) {
        Matrix identity = Matrix.createIdentity();
        List<List<Float>> toScale = identity.getMatrix();
        toScale.get(1).set(1, (float)Math.cos(radians));
        toScale.get(1).set(2, -(float)Math.sin(radians));
        toScale.get(2).set(1, (float)Math.sin(radians));
        toScale.get(2).set(2, (float)Math.cos(radians));

        return identity;
    }

    /**
     * Creates a Matrix for rotating points about the Y axis.<br/>
     * <strong>Made to work with 4x4 matrix explicitly.</strong>
     */
    public static Matrix rotateY(float radians) {
        Matrix identity = Matrix.createIdentity();
        List<List<Float>> toScale = identity.getMatrix();
        toScale.get(0).set(0, (float)Math.cos(radians));
        toScale.get(0).set(2, (float)Math.sin(radians));
        toScale.get(2).set(0, -(float)Math.sin(radians));
        toScale.get(2).set(2, (float)Math.cos(radians));

        return identity;
    }

    public static Matrix rotateZ(float radians) {
        Matrix identity = Matrix.createIdentity();
        List<List<Float>> toScale = identity.getMatrix();
        toScale.get(0).set(0, (float)Math.cos(radians));
        toScale.get(0).set(1, -(float)Math.sin(radians));
        toScale.get(1).set(0, (float)Math.sin(radians));
        toScale.get(1).set(1, (float)Math.cos(radians));

        return identity;
    }

    /**
     * Shearing (skewing) has the effect of making straight lines slanted. It changes
     * each value in a Tuple in proportion to the other components.</br>
     * <strong>Made to work with 4x4 matrix explicitly.</strong>
     * @param xToY x in proportion to y
     * @param xToZ x in proportion to z
     * @param yToX y in proportion to x
     * @param yToZ y in proportion to z
     * @param zToX z in proportion to x
     * @param zToY z in proportion to y
     */
    public static Matrix shearing(float xToY, float xToZ, float yToX, float yToZ, float zToX, float zToY) {
        Matrix identity = Matrix.createIdentity();
        List<List<Float>> toScale = identity.getMatrix();
        toScale.get(0).set(1, xToY);
        toScale.get(0).set(2, xToZ);
        toScale.get(1).set(0, yToX);
        toScale.get(1).set(2, yToZ);
        toScale.get(2).set(0, zToX);
        toScale.get(2).set(1, zToY);

        return identity;
    }
}

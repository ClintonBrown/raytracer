package raytracer;

public class Pixel {
    private Color _color;

    Pixel(){
        _color = new Color(0, 0, 0);
    }

    public Pixel(Color pixelColor){
        _color = pixelColor;
    }

    Color getColor(){
        return _color;
    }
}

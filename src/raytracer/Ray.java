package raytracer;

public class Ray {
    private Tuple _origin;
    private Tuple _direction;

    public Ray(Tuple origin, Tuple direction) {
        _origin = origin;
        _direction = direction;
    }

    /**
     * Finds a point along the ray at a given distance.
     */
    public Tuple getPositionAt(float distance) {
        return _direction.scale(distance).add(_origin);
    }

    /**
     * Applies the given transformation Matrix to this Ray.
     */
    public Ray transform(Matrix translation) {
        Tuple newOrigin = Tuple.createPoint(_origin.getX(), _origin.getY(), _origin.getZ());
        Tuple newDirection = Tuple.createVector(_direction.getX(), _direction.getY(), _direction.getZ());
        Ray newRay = new Ray(newOrigin, newDirection);

        // translate the origin by the translation matrix
        newRay.setOrigin(translation.multiply(newRay.getOrigin()));

        // translate the direction by the translation matrix
        newRay.setDirection(translation.multiply(newRay.getDirection()));

        return newRay;
    }

    public Tuple getOrigin(){ return _origin; }
    public Tuple getDirection(){ return _direction; }

    void setOrigin(Tuple newOrigin){ _origin = newOrigin; }
    void setDirection(Tuple newDirection){ _direction = newDirection; }
}

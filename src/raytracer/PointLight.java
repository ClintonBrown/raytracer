package raytracer;

public class PointLight {
    private Tuple _position;
    private Color _intensity;

    public PointLight(Tuple position, Color intensity) {
        _position = position;
        _intensity = intensity;
    }

    public Tuple getPosition() { return _position; }
    public Color getIntensity() { return _intensity; }
}

package raytracer;

import java.util.ArrayList;

public class Sphere {
    public final float RADIUS = 1;
    private Tuple _position;
    private Matrix _transform;
    private Material _material;

    public Sphere(){
        _position = Tuple.createPoint(0, 0, 0);
        _transform = Matrix.createIdentity();
        _material = new Material();
    }

    /**
     * Checks if the given ray intersects with this Sphere.
     * @return Collection of intersection objects where the ray intersects with the Sphere.
     */
    public ArrayList<Intersection> intersect(Ray ray) {
        ArrayList<Intersection> intersections = new ArrayList<>();

        // transform Ray by Sphere transform before calculation
        ray.setOrigin(_transform.getInverse().multiply(ray.getOrigin()));
        ray.setDirection(_transform.getInverse().multiply(ray.getDirection()));

        // Compute discriminant, if it's negative the ray misses and no
        // intersections have been found between the sphere and ray
        Tuple sphereToRay = ray.getOrigin().subtract(_position);
        float a = ray.getDirection().dotProduct(ray.getDirection());
        float b = 2.0f * ray.getDirection().dotProduct(sphereToRay);
        float c = sphereToRay.dotProduct(sphereToRay) - 1.0f;
        float discriminant = (float)Math.pow(b, 2) - 4.0f * a * c;

        // if intersections are found there will always be two calculated
        if (discriminant >= 0){
            float intersect1 = (-b - (float)Math.sqrt(discriminant)) / (2.0f * a);
            float intersect2 = (-b + (float)Math.sqrt(discriminant)) / (2.0f * a);
            intersections.add(new Intersection<>(intersect1, this));
            intersections.add(new Intersection<>(intersect2, this));
        }

        return intersections;
    }

    /**
     * The normal of a Sphere is the normalized vector perpendicular to the surface of
     * the Sphere at the intersection point.
     */
    public Tuple getNormalAt(Tuple worldPoint){
        // convert to object space
        Matrix inverse = _transform.getInverse();
        Tuple objectPoint = inverse.multiply(worldPoint);

        // calculate normal and then convert back to world space
        Tuple objectNormal = objectPoint.subtract(Tuple.createPoint(0, 0, 0));
        Tuple worldNormal = inverse.transpose().multiply(objectNormal);

        // You can either use a 3x3 submatrix of the transform to get the normal,
        // or you can just use the transform itself and set w to zero like this.
        worldNormal.setW(0);

        return worldNormal.normalize();
    }

    public Matrix getTransform(){ return _transform; }
    public Material getMaterial(){ return _material; }

    public void setTransform(Matrix newTransform){ _transform = newTransform; }
    public void setMaterial(Material material){ _material = material; }
}

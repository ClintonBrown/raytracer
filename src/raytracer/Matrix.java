package raytracer;

import tools.FloatTool;

import java.util.ArrayList;
import java.util.List;

public class Matrix {
    private List<List<Float>> _matrix;

    public Matrix(List<List<Float>> matrix) {
        _matrix = matrix;
    }

    /**
     * Without parameters return 4x4 identity, this is used most of
     * the time for ray tracing because of the dimensions of a Tuple.
     */
    public static Matrix createIdentity(){
        return createIdentity(4, 4);
    }

    public static Matrix createIdentity(int width, int height){
        List<List<Float>> identity = new ArrayList<>();
        for (int row = 0; row < height; row++){
            identity.add(new ArrayList<>());
            for (int col = 0; col < width; col++){
                if (row == col){
                    identity.get(row).add(1f);
                } else {
                    identity.get(row).add(0f);
                }
            }
        }
        return new Matrix(identity);
    }

    public boolean equals(Matrix matrixB){
        for (int row = 0; row < _matrix.size(); row++){
            for (int col = 0; col < _matrix.get(row).size(); col++){
                if (!FloatTool.areFloatsEqual(_matrix.get(row).get(col), matrixB.getMatrix().get(row).get(col))){
                    return false;
                }
            }
        }
        return true;
    }

    public Matrix multiply(Matrix matrixB) {
        List<List<Float>> products = new ArrayList<>();
        List<List<Float>> factors = matrixB.getMatrix();
        for (int row = 0; row < _matrix.size(); row++){
            products.add(new ArrayList<>());
            for (int col = 0; col < _matrix.get(row).size(); col++){
                float rowProduct = 0.0f;
                for (int i = 0; i < _matrix.size(); i++) {
                    rowProduct += _matrix.get(row).get(i) * factors.get(i).get(col);
                }
                products.get(row).add(rowProduct);
            }
        }
        return new Matrix(products);
    }

    public Tuple multiply(Tuple tuple) {
        float[] products = { 0, 0, 0, 0 };
        float[] factors = { tuple.getX(), tuple.getY(), tuple.getZ(), tuple.getW() };
        for (int row = 0; row < _matrix.size(); row++){
            float rowProduct = 0.0f;
            for (int col = 0; col < _matrix.get(row).size(); col++){
                    rowProduct += _matrix.get(row).get(col) * factors[col];
            }
            products[row] = rowProduct;
        }
        return new Tuple(products[0], products[1], products[2], products[3]);
    }

    public Matrix transpose() {
        List<List<Float>> transpose = new ArrayList<>();
        for (int row = 0; row < _matrix.size(); row++){
            transpose.add(new ArrayList<>());
            for (int col = 0; col < _matrix.get(row).size(); col++){
                transpose.get(row).add(_matrix.get(col).get(row));
            }
        }
        return new Matrix(transpose);
    }

    /**
     * Determinant of 2x2 matrix:<br/>
     * |a b|<br/>
     * |c d| = ad - bc
     */
    public float getDeterminant() {
        float determinant = 0;
        if (_matrix.size() == 2 && _matrix.get(0).size() == 2) {
            float a = _matrix.get(0).get(0);
            float b = _matrix.get(0).get(1);
            float c = _matrix.get(1).get(0);
            float d = _matrix.get(1).get(1);
            determinant = (a * d) - (b * c);
        } else {
            for (int col = 0; col < _matrix.get(0).size(); col++){
                determinant += _matrix.get(0).get(col) * this.getCofactor(0, col);
            }
        }
        return determinant;
    }

    /**
     * Returns the determinant of the submatrix given rowToRemove, colToRemove.
     */
    public float getMinor(int rowToRemove, int colToRemove) {
        Matrix submatrix = this.getSubmatrix(rowToRemove, colToRemove);
        return submatrix.getDeterminant();
    }

    /**
     * Returns the cofactor of the submatrix given rowToRemove, colToRemove. This
     * is the minor of the matrix if rowToRemove + colToRemove is even, and the
     * negation of the minor if it is odd and not zero.
     */
    public float getCofactor(int rowToRemove, int colToRemove) {
        int sum = rowToRemove + colToRemove;
        boolean negate = sum % 2 != 0;
        float minor = this.getMinor(rowToRemove, colToRemove);
        return (negate) ? -minor : minor;
    }

    /**
     * Returns a submatrix of this matrix with the specified row and column removed.
     */
    public Matrix getSubmatrix(int rowToRemove, int colToRemove) {
        List<List<Float>> submatrix = new ArrayList<>();
        for (int row = 0; row < _matrix.size(); row++){
            if (row != rowToRemove) {
                submatrix.add(new ArrayList<>());
                for (int col = 0; col < _matrix.get(row).size(); col++) {
                    // need to handle indices for when we are past the row/column being removed
                    if (col != colToRemove){
                        int currentRow = row;
                        if (row > rowToRemove){
                            currentRow -= 1;
                        }
                        submatrix.get(currentRow).add(_matrix.get(row).get(col));
                    }
                }
            }
        }
        return new Matrix(submatrix);
    }

    /**
     * Returns the inverse of this matrix if it is invertible, if it is
     * not invertible returns null.
     */
    public Matrix getInverse() {
        if (!this.isInvertible()){
            return null;
        }
        Matrix inverse = this.transpose();
        float determinant = this.getDeterminant();
        for (int row = 0; row < _matrix.size(); row++){
            for (int col = 0; col < _matrix.get(row).size(); col++){
                float cofactor = this.getCofactor(row, col);
                inverse.getMatrix().get(col).set(row, cofactor / determinant);
            }
        }
        return inverse;
    }

    public boolean isInvertible() {
        return this.getDeterminant() != 0;
    }

    public List<List<Float>> getMatrix(){ return _matrix; }
}

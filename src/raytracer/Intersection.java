package raytracer;

import java.util.ArrayList;

public class Intersection<E> {
    private float _distance;
    private E _object;

    public Intersection(float distance, E object) {
        _distance = distance;
        _object = object;
    }

    /**
     * Iterates through a list of Intersections and finds the hit,
     * the Intersection with the lowest non-negative distance.
     */
    public static Intersection hit(ArrayList<Intersection> intersections) {
        Intersection hit = null;
        for (Intersection intersection : intersections){
            // if the intersection's distance is less than hit's distance, make it the new hit
            if (intersection.getDistance() >= 0 && (hit == null || intersection.getDistance() < hit.getDistance())){
                hit = intersection;
            }
        }
        return hit;
    }

    public float getDistance(){ return _distance; }
    public E getObject(){ return _object; }
}

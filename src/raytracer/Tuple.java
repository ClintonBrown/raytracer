package raytracer;

import java.util.List;

import static tools.FloatTool.EPSILON;
import static tools.FloatTool.areFloatsEqual;

public class Tuple {
    private float _x;
    private float _y;
    private float _z;
    private float _w;

    /**
     * Create a Tuple with 4 values to represent a point or vector
     * @param w Will be 0 if this Tuple is a vector or 1 if it is a point.
     */
    public Tuple(float x, float y, float z, float w) {
        _x = x;
        _y = y;
        _z = z;
        _w = w;
    }

    public static Tuple createPoint(float x, float y, float z){
        return new Tuple(x, y, z, 1);
    }

    public static Tuple createVector(float x, float y, float z){
        return new Tuple(x, y, z, 0);
    }

    /**
     * Compare this Tuple to another to see if they match.
     * @param b The other Tuple object to compare this one against.
     */
    public boolean equals(Tuple b){
        return Math.abs(this.getX() - b.getX()) < EPSILON
            && Math.abs(this.getY() - b.getY()) < EPSILON
            && Math.abs(this.getZ() - b.getZ()) < EPSILON
            && Math.abs(this.getW() - b.getW()) < EPSILON
            && this.isPoint() == b.isPoint()
            && this.isVector() == b.isVector();
    }

    public Tuple add(Tuple b){
        float x = this.getX() + b.getX();
        float y = this.getY() + b.getY();
        float z = this.getZ() + b.getZ();
        float w = this.getW() + b.getW();

        return new Tuple(x, y, z, w);
    }

    public Tuple subtract(Tuple b){
        float x = this.getX() - b.getX();
        float y = this.getY() - b.getY();
        float z = this.getZ() - b.getZ();
        float w = this.getW() - b.getW();

        return new Tuple(x, y, z, w);
    }

    Tuple negate() {
        float x = -this.getX();
        float y = -this.getY();
        float z = -this.getZ();
        float w = -this.getW();

        return new Tuple(x, y, z, w);
    }

    public Tuple scale(float multiplier) {
        float x = this.getX() * multiplier;
        float y = this.getY() * multiplier;
        float z = this.getZ() * multiplier;
        float w = this.getW() * multiplier;

        return new Tuple(x, y, z, w);
    }

    Tuple divide(float divisor) {
        float x = this.getX() / divisor;
        float y = this.getY() / divisor;
        float z = this.getZ() / divisor;
        float w = this.getW() / divisor;

        return new Tuple(x, y, z, w);
    }

    float getMagnitude() {
        float xSquared = (float)Math.pow(this.getX(), 2);
        float ySquared = (float)Math.pow(this.getY(), 2);
        float zSquared = (float)Math.pow(this.getZ(), 2);
        float wSquared = (float)Math.pow(this.getW(), 2);

        return (float)Math.sqrt(xSquared + ySquared + zSquared + wSquared);
    }

    public Tuple normalize() {
        float x = this.getX() / this.getMagnitude();
        float y = this.getY() / this.getMagnitude();
        float z = this.getZ() / this.getMagnitude();
        float w = this.getW() / this.getMagnitude();

        return new Tuple(x, y, z, w);
    }

    float dotProduct(Tuple vector2) {
        return this.getX() * vector2.getX()
             + this.getY() * vector2.getY()
             + this.getZ() * vector2.getZ()
             + this.getW() * vector2.getW();
    }

    Tuple crossProduct(Tuple vector2) {
        return Tuple.createVector((this.getY() * vector2.getZ()) - (this.getZ() * vector2.getY()),
                                  (this.getZ() * vector2.getX()) - (this.getX() * vector2.getZ()),
                                  (this.getX() * vector2.getY()) - (this.getY() * vector2.getX()));
    }

    public Tuple reflect(Tuple normal) {
        if (!normal.isVector()){
            throw new UnsupportedOperationException("Tuple.reflect does not support non-Vector types!");
        }

        Tuple scaled = normal.scale(2).scale(this.dotProduct(normal));
        return this.subtract(scaled);
    }

    public float getX(){ return _x; }
    public float getY(){ return _y; }
    float getZ(){ return _z; }
    float getW(){ return _w; }

    void setX(float val){ _x = val; }
    void setY(float val){ _y = val; }
    void setZ(float val){ _z = val; }
    void setW(float val){ _w = val; }

    boolean isPoint(){ return areFloatsEqual(this.getW(), 1.0f); }
    boolean isVector(){ return areFloatsEqual(this.getW(), 0.0f); }
}

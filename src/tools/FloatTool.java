package tools;

public class FloatTool {
    public final static float EPSILON = 0.0001f;

    public static boolean areFloatsEqual(float a, float b){
        return Math.abs(a - b) < EPSILON;
    }
}

package tools;

/**
 * Some libraries, like Cucumber, may sometimes give you an index as a number.
 * This class can be used to get an index from that number.
 */
public class IndexTool {
    private int _y;
    private int _x;

    public IndexTool(float indicesAsFloat){
        String indices = Float.toString(indicesAsFloat);
        if (indices.charAt(1) != '.'){
            _y = Character.getNumericValue(indices.charAt(0));
            _x = Character.getNumericValue(indices.charAt(1));
        } else {
            _y = 0;
            _x = Character.getNumericValue(indices.charAt(0));
        }
    }

    public int getX(){ return _x; }
    public int getY(){ return _y; }
}

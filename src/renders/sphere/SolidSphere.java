package renders.sphere;

import raytracer.*;

import java.util.ArrayList;

public class SolidSphere {
    public static void main(String[] args) {
        // set up drawing canvas
        int canvasPixels = 800;
        Canvas canvas = new Canvas(canvasPixels, canvasPixels);

        // set up wall
        float wallSize = 7;
        float half = wallSize / 2;
        float wallZ = 10;

        // set up pixel size
        float pixelSize = wallSize / canvasPixels;

        for (int draw = 0; draw < 2; draw++) {
            // create the sphere
            Sphere shape = new Sphere();
            shape.setMaterial(new Material());
            shape.getMaterial().setColor(new Color(0.75f, 0.5f, 1));

            // draw first sphere in upper right scaled to 75%, second in lower left
            if (draw == 0){
                shape.setTransform(Transform.translation(0.45f, 0.45f, 1f).multiply(Transform.scaling(0.75f, 0.75f, 0.75f)));
            } else {
                shape.setTransform(Transform.translation(-0.37f, -0.37f, 1f));
            }

            // create light source
            Tuple lightPosition = Tuple.createPoint(-10, 10, -10);
            Color lightColor = new Color(1, 1, 1);
            PointLight light = new PointLight(lightPosition, lightColor);

            for (int y = 0; y < canvasPixels; y++) {
                // compute world coordinates
                float worldY = half - pixelSize * y;
                for (int x = 0; x < canvasPixels; x++) {
                    float worldX = -half + pixelSize * x;
                    Tuple rayTarget = Tuple.createPoint(worldX, worldY, wallZ);

                    // create the ray
                    Tuple rayOrigin = Tuple.createPoint(0, 0, -5);
                    Tuple rayDirection = rayTarget.subtract(rayOrigin).normalize();
                    Ray ray = new Ray(rayOrigin, rayDirection);

                    // calculate hits
                    ArrayList<Intersection> intersections = shape.intersect(ray);
                    if (!intersections.isEmpty()) {
                        Intersection hit = Intersection.hit(intersections);

                        // normalize ray direction
                        Tuple point = ray.getPositionAt(hit.getDistance());
                        Tuple normal = shape.getNormalAt(point);
                        Tuple eye = rayDirection.scale(-1);

                        Material objectMaterial = shape.getMaterial();
                        Color pixelColor = objectMaterial.addLighting(light, point, eye, normal);
                        canvas.setPixel(x, y, new Pixel(pixelColor));
                    }
                }
            }
        }
        canvas.writeToPPM("images/solid_sphere");
    }
}

package renders.cannon;

import raytracer.Tuple;

public class Environment {
    private Tuple _gravity;
    private Tuple _wind;

    public Environment(Tuple gravityVector, Tuple windVector){
        _gravity = gravityVector;
        _wind = windVector;
    }

    Projectile tick(Projectile proj){
        Tuple position = proj.getPosition().add(proj.getVelocity());
        Tuple velocity = proj.getVelocity().add(_gravity).add(_wind);

        return new Projectile(position, velocity);
    }
}
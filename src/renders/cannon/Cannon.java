package renders.cannon;

import raytracer.Canvas;
import raytracer.Color;
import raytracer.Pixel;
import raytracer.Tuple;

public class Cannon {
    public static void main(String[] args) {
        // set up the cannonball projectile
        Tuple position = Tuple.createPoint(0, 1, 0);
        Tuple velocity = Tuple.createVector(1, 1.8f, 0).normalize().scale(11.25f);
        Projectile cannonball = new Projectile(position, velocity);

        // set up the environment
        Tuple gravity = Tuple.createVector(0, -0.1f, 0);
        Tuple wind = Tuple.createVector(-0.01f, 0, 0);
        Environment environment = new Environment(gravity, wind);

        Canvas canvas = new Canvas(900, 550);

        // move the cannonball until it hits the ground
        for (int count = 0; cannonball.getPosition().getY() > 0; count++){
            printProjectilePosition(cannonball, String.valueOf(count));
            int canvasX = Math.round(cannonball.getPosition().getX());
            int canvasY = Math.round(canvas.getHeight() - cannonball.getPosition().getY());
            canvas.setPixel(canvasX, canvasY, new Pixel(new Color(1, 0.0f, 0.0f)));
            cannonball = environment.tick(cannonball);
        }
        printProjectilePosition(cannonball, "Final");
        canvas.writeToPPM("images/cannon");
    }

    private static void printProjectilePosition(Projectile projectile, String tickCount){
        System.out.print(tickCount + " - ");
        System.out.println("Projectile Position: " + projectile.getPosition().getY());

    }
}

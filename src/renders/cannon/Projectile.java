package renders.cannon;

import raytracer.Tuple;

public class Projectile {
    private Tuple _position;
    private Tuple _velocity;

    /**
     * Creates a projectile.
     * @param point The point for the projectiles starting position.
     * @param vector The velocity vector of the projectile.
     */
    public Projectile(Tuple point, Tuple vector){
        _position = point;
        _velocity = vector;
    }

    Tuple getPosition() {
        return _position;
    }

    Tuple getVelocity() {
        return _velocity;
    }
}

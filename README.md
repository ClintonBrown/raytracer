**RayTracer**

This project is a Java implementation of a Ray Tracer. It is being developed using TDD following the book [The Ray Tracer Challenge](https://pragprog.com/book/jbtracer/the-ray-tracer-challenge) by Jamis Buck.

The current goal of this project is to create a Ray Tracer that can create scenes and output them as ppm image files.
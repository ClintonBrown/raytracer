Feature: Materials

  Background:
    Given mat ← material
    And matPosition ← point at 0, 0, 0

  Scenario: The default material
    Given mat ← material
    Then mat.getColor = color of 1, 1, 1
      And mat.getAmbient = 0.1
      And mat.getDiffuse = 0.9
      And mat.getSpecular = 0.9
      And mat.getShininess = 200.0

  # Lighting Materials

  Scenario: Lighting with the eye between the light and the surface
    Given eyev ← vector of 0, 0, -1
      And normalv ← vector of 0, 0, -1
      And light ← PointLight at point 0, 0, -10 with color 1, 1, 1
    When result ← mat.addLighting with light, matPosition, eyev, normalv
    Then result = color of 1.9, 1.9, 1.9

  Scenario: Lighting with the eye between light and surface, eye offset 45°
    Given eyev ← vector of 0, square root of 2 over 2, negated square root of 2 over 2
    And normalv ← vector of 0, 0, -1
    And light ← PointLight at point 0, 0, -10 with color 1, 1, 1
    When result ← mat.addLighting with light, matPosition, eyev, normalv
    Then result = color of 1.0, 1.0, 1.0

  Scenario: Lighting with eye opposite surface, light offset 45°
    Given eyev ← vector of 0, 0, -1
    And normalv ← vector of 0, 0, -1
    And light ← PointLight at point 0, 10, -10 with color 1, 1, 1
    When result ← mat.addLighting with light, matPosition, eyev, normalv
    Then result = color of 0.7364, 0.7364, 0.7364

  Scenario: Lighting with eye in the path of the reflection vector
    Given eyev ← vector of 0, negated square root of 2 over 2, negated square root of 2 over 2
    And normalv ← vector of 0, 0, -1
    And light ← PointLight at point 0, 10, -10 with color 1, 1, 1
    When result ← mat.addLighting with light, matPosition, eyev, normalv
    Then result = color of 1.6364, 1.6364, 1.6364

  Scenario: Lighting with the light behind the surface
    Given eyev ← vector of 0, 0, -1
    And normalv ← vector of 0, 0, -1
    And light ← PointLight at point 0, 0, 10 with color 1, 1, 1
    When result ← mat.addLighting with light, matPosition, eyev, normalv
    Then result = color of 0.1, 0.1, 0.1

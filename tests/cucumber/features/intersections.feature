Feature: Intersections

  Scenario: An intersection encapsulates t and object
    Given s ← sphere to intersect
    When i ← intersection at distance 3.5 with object s
    Then i.getDistance = 3.5
    And i.getObject = s

  Scenario: Aggregating intersections
    Given s ← sphere to intersect
    And i1 ← intersection at distance 1 with object s
    And i2 ← intersection at distance 2 with object s
    When xs ← collection of intersections i1, i2
    Then xs.size is 2
    And xs[0].distance = 1
    And xs[1].distance = 2

  Scenario: The hit, when all intersections have positive t
    Given s ← sphere to intersect
    And i1 ← intersection at distance 1 with object s
    And i2 ← intersection at distance 2 with object s
    And xs ← collection of intersections i2, i1
    When i ← hit from xs
    Then i = i1
    
  Scenario: The hit, when some intersections have negative t
    Given s ← sphere to intersect
    And i1 ← intersection at distance -1 with object s
    And i2 ← intersection at distance 1 with object s
    And xs ← collection of intersections i2, i1
    When i ← hit from xs
    Then i = i2
    
  Scenario: The hit, when all intersections have negative t
    Given s ← sphere to intersect
    And i1 ← intersection at distance -2 with object s
    And i2 ← intersection at distance -1 with object s
    And xs ← collection of intersections i2, i1
    When i ← hit from xs
    Then i is nothing
    
  Scenario: The hit is always the lowest nonnegative intersection
    Given s ← sphere to intersect
    And i1 ← intersection at distance 5 with object s
    And i2 ← intersection at distance 7 with object s
    And i3 ← intersection at distance -3 with object s
    And i4 ← intersection at distance 2 with object s
    And xs ← collection of intersections i1, i2, i3, i4
    When i ← hit from xs
    Then i = i4


Feature: Tuples

  # Reflecting vectors

  Scenario: Reflecting a vector approaching at 45°
    Given vector ← vector of 1, -1, 0
    And normal ← vector of 0, 1, 0
    When reflection ← vector.reflect of normal
    Then reflection = vector of 1, 1, 0

  Scenario: Reflecting a vector off a slanted surface
    Given vector ← vector of 0, -1, 0
    And normal ← vector of square root 2 over 2, square root of 2 over 2, 0
    When reflection ← vector.reflect of normal
    Then reflection = vector of 1, 0, 0

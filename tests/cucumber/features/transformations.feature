Feature: Transformations

  Scenario: Multiplying by a translation matrix
    Given transform ← translation of 5, -3, 2
    And p ← point at -3, 4, 5
    Then transform * p = point at 2, 1, 7

  Scenario: Multiplying by the inverse of a translation matrix
    Given transform ← translation of 5, -3, 2
    And inv ← inverse of transform
    And p ← point at -3, 4, 5
    Then inv * p = point at -8, 7, 3

  Scenario: Translation does not affect vectors
    Given transform ← translation of 5, -3, 2
    And v ← vector at -3, 4, 5
    Then transform * v = v

  Scenario: A scaling matrix applied to a point
    Given transform ← scaling of 2, 3, 4
    And p ← point at -4, 6, 8
    Then transform * p = point at -8, 18, 32

  Scenario: A scaling matrix applied to a vector
    Given transform ← scaling of 2, 3, 4
    And v ← vector at -4, 6, 8
    Then transform * v = vector at -8, 18, 32

  Scenario: Multiplying by the inverse of a scaling matrix
    Given transform ← scaling of 2, 3, 4
    And inv ← inverse of transform
    And v ← vector at -4, 6, 8
    Then inv * v = vector at -2, 2, 2
  Scenario: Reflection is scaling by a negative value
    Given transform ← scaling of -1, 1, 1
    And p ← point at 2, 3, 4
    Then transform * p = point at -2, 3, 4

  Scenario: Rotating a point around the x axis
    Given p ← point at 0, 1, 0
    And half_quarter ← rotation about x of π divided by 4
    And full_quarter ← rotation about x of π divided by 2
    Then half_quarter * p = point where x is square root of 0 over 0, y is square root of 2 over 2, and z is square root of 2 over 2
    And full_quarter * p = point at 0, 0, 1

  Scenario: The inverse of an x-rotation rotates in the opposite direction
    Given p ← point at 0, 1, 0
    And half_quarter ← rotation about x of π divided by 4
    And inv ← inverse(half_quarter)
    Then inv * p = point where x is 0, y is the square root of 2 over 2, and z is the negated square root of 2 over 2

  Scenario: Rotating a point around the y axis
    Given p ← point at 0, 0, 1
    And half_quarter ← rotation about y of π divided by 4
    And full_quarter ← rotation about y of π divided by 2
    Then half_quarter * p = point where x is square root of 2 over 2, y is square root of 0 over 0, and z is square root of 2 over 2
    And full_quarter * p = point at 1, 0, 0

  Scenario: Rotating a point around the z axis
    Given p ← point at 0, 1, 0
    And half_quarter ← rotation about z of π divided by 4
    And full_quarter ← rotation about z of π divided by 2
    Then half_quarter * p = point where x is negated square root of 2 over 2, y is square root of 2 over 2, and z is square root of 0 over 0
    And full_quarter * p = point at -1, 0, 0

  Scenario: A shearing transformation moves x in proportion to y
    Given transform ← shearing of 1, 0, 0, 0, 0, 0
    And p ← point at 2, 3, 4
    Then transform * p = point at 5, 3, 4

  Scenario: A shearing transformation moves x in proportion to z
    Given transform ← shearing of 0, 1, 0, 0, 0, 0
    And p ← point at 2, 3, 4
    Then transform * p = point at 6, 3, 4

  Scenario: A shearing transformation moves y in proportion to x
    Given transform ← shearing of 0, 0, 1, 0, 0, 0
    And p ← point at 2, 3, 4
    Then transform * p = point at 2, 5, 4

  Scenario: A shearing transformation moves y in proportion to z
    Given transform ← shearing of 0, 0, 0, 1, 0, 0
    And p ← point at 2, 3, 4
    Then transform * p = point at 2, 7, 4

  Scenario: A shearing transformation moves z in proportion to x
    Given transform ← shearing of 0, 0, 0, 0, 1, 0
    And p ← point at 2, 3, 4
    Then transform * p = point at 2, 3, 6

  Scenario: A shearing transformation moves z in proportion to y
    Given transform ← shearing of 0, 0, 0, 0, 0, 1
    And p ← point at 2, 3, 4
    Then transform * p = point at 2, 3, 7

  Scenario: Individual transformations are applied in sequence
    Given p ← point at 1, 0, 1
    And A ← rotation about x of π divided by 2
    And B ← scaling of 5, 5, 5
    And C ← translation of 10, 5, 7
    # apply rotation first
    When p2 ← A * p
    Then p2 = point at 1, -1, 0
    # then apply scaling
    When p3 ← B * p2
    Then p3 = point at 5, -5, 0
    # then apply translation
    When p4 ← C * p3
    Then p4 = point at 15, 0, 7
  Scenario: Chained transformations must be applied in reverse order
    Given p ← point at 1, 0, 1
    And A ← rotation about x of π divided by 2
    And B ← scaling of 5, 5, 5
    And C ← translation of 10, 5, 7
    When T ← C * B * A
    Then T * p = point at 15, 0, 7

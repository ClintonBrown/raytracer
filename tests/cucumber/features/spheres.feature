Feature: Spheres
  #Uses steps from RaysFeatureSteps

  # Ray/Sphere intersections

  Scenario: A ray intersects a sphere at two points
    Given r ← ray with origin at 0, 0, -5, and direction of 0, 0, 1
    And s ← sphere
    When xs ← collection of intersections between s and r
    Then xs.count = 2
    And xs[0] = 4.0
    And xs[1] = 6.0

  Scenario: A ray intersects a sphere at a tangent
    Given r ← ray with origin at 0, 1, -5, and direction of 0, 0, 1
    And s ← sphere
    When xs ← collection of intersections between s and r
    Then xs.count = 2
    And xs[0] = 5.0
    And xs[1] = 5.0

  Scenario: A ray misses a sphere
    Given r ← ray with origin at 0, 2, -5, and direction of 0, 0, 1
    And s ← sphere
    When xs ← collection of intersections between s and r
    Then xs.count = 0

  Scenario: A ray originates inside a sphere
    Given r ← ray with origin at 0, 0, 0, and direction of 0, 0, 1
    And s ← sphere
    When xs ← collection of intersections between s and r
    Then xs.count = 2
    And xs[0] = -1.0
    And xs[1] = 1.0

  Scenario: A sphere is behind a ray
    Given r ← ray with origin at 0, 0, 5, and direction of 0, 0, 1
    And s ← sphere
    When xs ← collection of intersections between s and r
    Then xs.count = 2
    And xs[0] = -6.0
    And xs[1] = -4.0

  Scenario: Intersect sets the object on the intersection
    Given r ← ray with origin at 0, 0, -5, and direction of 0, 0, 1
    And s ← sphere
    When xs ← collection of intersections between s and r
    Then xs.count = 2
    And xs[0].getObject = s
    And xs[1].getObject = s

  Scenario: A sphere's default transformation
    Given s ← sphere
    Then s.transform = identity_matrix

  Scenario: Changing a sphere's transformation
    Given s ← sphere
    And m ← translation of 2, 3, 4
    When s.set_transform = s, m
    Then s.getTransform = m

  Scenario: Intersecting a scaled sphere with a ray
    Given r ← ray with origin at 0, 0, -5, and direction of 0, 0, 1
    And s ← sphere
    When s.set_transform to scaling of 2, 2, 2
    And xs ← collection of intersections between s and r
    Then xs.count = 2
    And xs[0] = 3
    And xs[1] = 7

  Scenario: Intersecting a translated sphere with a ray
    Given r ← ray with origin at 0, 0, -5, and direction of 0, 0, 1
    And s ← sphere
    When s.set_transform to translation of 5, 0, 0
    And xs ← collection of intersections between s and r
    Then xs.count = 0

  # Computing normals on a sphere

  Scenario: The normal on a sphere at a point on the x axis
    Given s ← sphere
    When normal ← s.getNormalAt point 1, 0, 0
    Then normal = vector of 1, 0, 0

  Scenario: The normal on a sphere at a point on the y axis
    Given s ← sphere
    When normal ← s.getNormalAt point 0, 1, 0
    Then normal = vector of 0, 1, 0

  Scenario: The normal on a sphere at a point on the z axis
    Given s ← sphere
    When normal ← s.getNormalAt point 0, 0, 1
    Then normal = vector of 0, 0, 1

  Scenario: The normal on a sphere at a nonaxial point
    Given s ← sphere
    When normal ← s.getNormalAt point square root of 3 over 3, square root of 3 over 3, square root of 3 over 3
    Then normal = vector of square root of 3 over 3, square root of 3 over 3, square root of 3 over 3

  Scenario: The normal is a normalized vector
    Given s ← sphere
    When normal ← s.getNormalAt point square root of 3 over 3, square root of 3 over 3, square root of 3 over 3
    Then normal = normal.normalize

  # Computing normals on transformed spheres

  Scenario: Computing the normal on a translated sphere
    Given s ← sphere
    And s.set_transform to translation of 0, 1, 0
    When normal ← s.getNormalAt point 0, 1.70711, -0.70711
    Then normal = vector of 0, 0.70711, -0.70711
  Scenario: Computing the normal on a transformed sphere
    Given s ← sphere
    And m ← scaling of 1, 0.5, 1 * rotation_z of π over 5
    And s.set_transform = s, m
    When normal ← s.getNormalAt point 0, square root of 2 over 2, negated square root of 2 over 2
    Then normal = vector of 0, 0.97014, -0.24254

  # Sphere materials

  Scenario: A sphere has a default material
    Given sphere ← new Sphere
    When mat ← s.getMaterial
    Then mat = default Material
  Scenario: A sphere may be assigned a material
    Given sphere ← new Sphere
      And mat ← new Material
      And mat.ambient ← 1
    When sphere.setMaterial ← mat
    Then sphere.getMaterial = mat

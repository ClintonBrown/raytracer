Feature: Lights

  # Point lights

  Scenario: A point light has a position and intensity
    Given intensity ← color of 1, 1, 1
      And position ← point at 0, 0, 0
    When light ← PointLight at position with intensity
    Then light.getPosition = position
      And light.getIntensity = intensity

Feature: Rays

  Scenario: Creating and querying a ray
    Given origin ← point at 1, 2, 3
    And direction ← vector at 4, 5, 6
    When r ← ray at origin, with direction
    Then r.getOrigin = origin
    And r.getDirection = direction

  Scenario: Computing a point from a distance
    Given r ← ray with origin at 2, 3, 4, and direction of 1, 0, 0
    Then position along r at distance of 0 = point at 2, 3, 4
    And position along r at distance of 1 = point at 3, 3, 4
    And position along r at distance of -1 = point at 1, 3, 4
    And position along r at distance of 2.5 = point at 4.5, 3, 4

  Scenario: Translating a ray
    Given r ← ray with origin at 1, 2, 3, and direction of 0, 1, 0
    And m ← translation of 3, 4, 5
    When r2 ← transform by r, m
    Then r2.origin = point at 4, 6, 8
    And r2.direction = vector of 0, 1, 0

  Scenario: Scaling a ray
    Given r ← ray with origin at 1, 2, 3, and direction of 0, 1, 0
    And m ← scaling of 2, 3, 4
    When r2 ← transform by r, m
    Then r2.origin = point at 2, 6, 12
    And r2.direction = vector of 0, 3, 0

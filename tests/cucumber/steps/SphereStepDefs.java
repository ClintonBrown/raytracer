package cucumber.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import raytracer.Material;
import raytracer.Sphere;
import tools.FloatTool;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class SphereStepDefs {
    private Sphere sphere;
    private Material mat;

    @Given("sphere ← new Sphere")
    public void sSphere() {
        sphere = new Sphere();
    }

    @When("mat ← s.getMaterial")
    public void matSGetMaterial() {
        mat = sphere.getMaterial();
    }

    @Then("mat = default Material")
    public void matDefaultMaterial() {
        assertTrue(FloatTool.areFloatsEqual(.1f, mat.getAmbient()));
        assertTrue(FloatTool.areFloatsEqual(0.9f, mat.getDiffuse()));
        assertTrue(FloatTool.areFloatsEqual(0.9f, mat.getSpecular()));
        assertTrue(FloatTool.areFloatsEqual(200, mat.getShininess()));
    }

    @And("mat ← new Material")
    public void matNewMaterial() {
        mat = new Material();
    }

    @And("mat.ambient ← {int}")
    public void matAmbient(int ambient) {
        mat.setAmbient(ambient);
    }

    @When("sphere.setMaterial ← mat")
    public void sphereSetMaterialMat() {
        sphere.setMaterial(mat);
    }

    @Then("sphere.getMaterial = mat")
    public void sphereGetMaterialMat() {
        assertEquals(mat, sphere.getMaterial());
    }
}

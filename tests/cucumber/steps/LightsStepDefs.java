package cucumber.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import raytracer.Color;
import raytracer.PointLight;
import raytracer.Tuple;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class LightsStepDefs {
    private Color intensity;
    private Tuple position;
    private PointLight light;

    @Given("intensity ← color of {int}, {int}, {int}")
    public void intensityColorOf(int r, int g, int b) {
        intensity = new Color(r, g, b);
    }

    @And("position ← point at {int}, {int}, {int}")
    public void positionPointAt(int x, int y, int z) {
        position = Tuple.createPoint(x, y, z);
    }

    @When("light ← PointLight at position with intensity")
    public void lightPointLightAtPositionWithIntensity() {
        light = new PointLight(position, intensity);
    }

    @Then("light.getPosition = position")
    public void lightPositionPosition() {
        assertTrue(light.getPosition().equals(position));
    }

    @And("light.getIntensity = intensity")
    public void lightGetIntensityIntensity() {
        assertTrue(light.getIntensity().equals(intensity));
    }
}

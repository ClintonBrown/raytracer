package cucumber.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import raytracer.*;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class RaysStepDefs {
    private Tuple origin;
    private Tuple direction;
    private Ray r;
    private Ray r2;
    private Sphere s;
    private ArrayList<Intersection> xs;
    private Matrix m;
    private Tuple normal;


    @Given("origin ← point at {int}, {int}, {int}")
    public void originPoint(int x, int y, int z) {
        origin = Tuple.createPoint(x, y, z);
    }

    @And("direction ← vector at {int}, {int}, {int}")
    public void directionVector(int x, int y, int z) {
        direction = Tuple.createVector(x, y, z);
    }

    @When("r ← ray at origin, with direction")
    public void rRayAtOriginWithDirection() {
        r = new Ray(origin, direction);
    }

    @Then("r.getOrigin = origin")
    public void rGetOriginOrigin() {
        assertTrue(r.getOrigin().equals(origin));
    }

    @And("r.getDirection = direction")
    public void rDirectionDirection() {
        assertTrue(r.getDirection().equals(direction));
    }

    @Given("r ← ray with origin at {int}, {int}, {int}, and direction of {int}, {int}, {int}")
    public void rRayWithOriginAtAndDirectionOf(int originX, int originY, int originZ,
                                               int directionX, int directionY, int directionZ) {
        origin = Tuple.createPoint(originX, originY, originZ);
        direction = Tuple.createVector(directionX, directionY, directionZ);
        r = new Ray(origin, direction);
    }

    @Then("position along r at distance of {int} = point at {int}, {int}, {int}")
    public void positionAlongRAtDistanceOfPointAt(int distance, int x, int y, int z) {
        assertTrue(r.getPositionAt(distance).equals(Tuple.createPoint(x, y, z)));
    }

    @And("position along r at distance of {float} = point at {float}, {int}, {int}")
    public void positionAlongRAtDistanceOfPointAt(float distance, float x, int y, int z) {
        assertTrue(r.getPositionAt(distance).equals(Tuple.createPoint(x, y, z)));
    }

    @And("s ← sphere")
    public void sSphere() {
        s = new Sphere();
    }

    @When("xs ← collection of intersections between s and r")
    public void xsCollectionOfIntersectionsBetweenSAndR() {
        xs = s.intersect(r);
    }

    @Then("xs.count = {int}")
    public void xsCount(int intersectCount) {
        assertEquals(intersectCount, s.intersect(r).size());
    }

    @And("xs[{int}] = {float}")
    public void xs(int i, float distance) {
        assertEquals(distance, xs.get(i).getDistance());
    }

    @And("xs[{int}] = {int}")
    public void xs(int i, int distance) {
        assertEquals(distance, xs.get(i).getDistance());
    }

    @And("xs[{int}].getObject = s")
    public void xsGetObjectS(int i) {
        assertEquals(s, xs.get(i).getObject());
    }

    @And("m ← translation of {int}, {int}, {int}")
    public void mTranslationOf(int x, int y, int z) {
        m = Transform.translation(x, y, z);
    }

    @When("r2 ← transform by r, m")
    public void rTransformByRM() {
        r2 = r.transform(m);
    }

    @Then("r2.origin = point at {int}, {int}, {int}")
    public void r2OriginPointAt(int x, int y, int z) {
        assertTrue(r2.getOrigin().equals(Tuple.createPoint(x, y, z)));
    }

    @And("r2.direction = vector of {int}, {int}, {int}")
    public void rDirectionVectorOf(int x, int y, int z) {
        assertTrue(r2.getDirection().equals(Tuple.createVector(x, y, z)));
    }

    @And("m ← scaling of {int}, {int}, {int}")
    public void mScalingOf(int x, int y, int z) {
        m = Transform.scaling(x, y, z);
    }

    @Then("s.transform = identity_matrix")
    public void sTransformIdentity_matrix() {

    }

    @When("s.set_transform = s, m")
    public void sSet_transformSM() {
        s.setTransform(m);
    }

    @Then("s.getTransform = m")
    public void sGetTransformM() {
        assertEquals(m, s.getTransform());
    }

    @When("s.set_transform to scaling of {int}, {int}, {int}")
    public void sSet_transformToScalingOf(int x, int y, int z) {
        Matrix newTransform = Transform.scaling(x, y, z);
        s.setTransform(newTransform);
    }

    @When("s.set_transform to translation of {int}, {int}, {int}")
    public void sSet_transformToTranslationOf(int x, int y, int z) {
        s.setTransform(Transform.translation(x, y, z));
    }

    @When("normal ← s.getNormalAt point {int}, {int}, {int}")
    public void normalSGetNormalAtPoint(int x, int y, int z) {
        normal = s.getNormalAt(Tuple.createPoint(x, y, z));
    }

    @Then("normal = vector of {int}, {int}, {int}")
    public void normalVectorOf(int x, int y, int z) {
        assertTrue(normal.equals(Tuple.createVector(x, y, z)));
    }

    @When("normal ← s.getNormalAt point square root of {int} over {int}, square root of {int} over {int}, square root of {int} over {int}")
    public void normalSGetNormalAtPointSquareRootOfOverSquareRootOfOverSquareRootOfOver(int x1, int x2, int y1, int y2, int z1, int z2) {
        float x = (float)Math.sqrt(x1) / x2;
        float y = (float)Math.sqrt(y1) / y2;
        float z = (float)Math.sqrt(z1) / z2;
        normal = s.getNormalAt(Tuple.createPoint(x, y, z));
    }

    @Then("normal = vector of square root of {int} over {int}, square root of {int} over {int}, square root of {int} over {int}")
    public void normalVectorOfSquareRootOfOverSquareRootOfOverSquareRootOfOver(int x1, int x2, int y1, int y2, int z1, int z2) {
        float x = (float)Math.sqrt(x1) / x2;
        float y = (float)Math.sqrt(y1) / y2;
        float z = (float)Math.sqrt(z1) / z2;
        assertTrue(normal.equals(Tuple.createVector(x, y, z)));
    }

    @Then("normal = normal.normalize")
    public void normalNormalNormalize() {
        assertTrue(normal.equals(normal.normalize()));
    }

    @When("normal ← s.getNormalAt point {int}, {float}, {float}")
    public void normalSGetNormalAtPoint(int x, float y, float z) {
        normal = s.getNormalAt(Tuple.createPoint(x, y, z));
    }

    @Then("normal = vector of {int}, {float}, {float}")
    public void normalVectorOf(int x, float y, float z) {
        assertTrue(normal.equals(Tuple.createVector(x, y, z)));
    }

    @And("m ← scaling of {int}, {float}, {int} * rotation_z of π over {int}")
    public void mScalingOfRotation_zOfΠOver(int x, float y, float z, int radianDivisor) {
        m = Transform.scaling(x, y, z).multiply(Transform.rotateZ((float)Math.PI / radianDivisor));
    }

    @When("normal ← s.getNormalAt point {int}, square root of {int} over {int}, negated square root of {int} over {int}")
    public void normalSGetNormalAtPointSquareRootOfOverNegatedSquareRootOfOver(int x, int y1, int y2, int z1, int z2) {
        float y = (float)Math.sqrt(y1) / y2;
        float z = -(float)Math.sqrt(z1) / z2;
        normal = s.getNormalAt(Tuple.createPoint(x, y, z));
    }
}

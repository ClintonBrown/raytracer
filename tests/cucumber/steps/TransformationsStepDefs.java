package cucumber.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import raytracer.Matrix;
import raytracer.Transform;
import raytracer.Tuple;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class TransformationsStepDefs {
    private final float PI = (float)Math.PI;
    private Matrix transform;
    private Tuple point;
    private Tuple point2;
    private Tuple point3;
    private Tuple point4;
    private Tuple vector;
    private Matrix inverse;
    private Matrix halfQuarter;
    private Matrix fullQuarter;
    private Matrix matrixA;
    private Matrix matrixB;
    private Matrix matrixC;
    private Matrix matrixT;

    @Given("transform ← translation of {int}, {int}, {int}")
    public void transformTranslation(int x, int y, int z) {
        transform = Transform.translation(x, y, z);
    }

    @And("p ← point at {int}, {int}, {int}")
    public void pPoint(int x, int y, int z) {
        point = Tuple.createPoint(x, y, z);
    }

    @Then("transform * p = point at {int}, {int}, {int}")
    public void transformPPoint(int x, int y, int z) {
        Tuple expected = Tuple.createPoint(x, y, z);
        assertTrue(transform.multiply(point).equals(expected));
    }

    @And("inv ← inverse of transform")
    public void invInverseTransform() {
        inverse = transform.getInverse();
    }

    @Then("inv * p = point at {int}, {int}, {int}")
    public void invPPoint(int x, int y, int z) {
        Tuple expected = Tuple.createPoint(x, y, z);
        assertTrue(inverse.multiply(point).equals(expected));
    }

    @And("v ← vector at {int}, {int}, {int}")
    public void vVector(int x, int y, int z) {
        vector = Tuple.createVector(x, y, z);
    }

    @Then("transform * v = v")
    public void transformVV() {
        assertTrue(transform.multiply(vector).equals(vector));
    }

    @Given("transform ← scaling of {int}, {int}, {int}")
    public void transformScaling(int x, int y, int z) {
        transform = Transform.scaling(x, y, z);
    }

    @Then("inv * v = vector at {int}, {int}, {int}")
    public void invVVector(int x, int y, int z) {
        assertTrue(inverse.multiply(vector).equals(Tuple.createVector(x, y, z)));
    }

    @Then("transform * v = vector at {int}, {int}, {int}")
    public void transformVVector(int x, int y, int z) {
        assertTrue(transform.multiply(vector).equals(Tuple.createVector(x, y, z)));
    }


    @And("half_quarter ← rotation about x of π divided by {int}")
    public void half_quarterRotation_x(int divisor) {
        halfQuarter = Transform.rotateX(PI/divisor);
    }

    @And("full_quarter ← rotation about x of π divided by {int}")
    public void full_quarterRotation_x(int divisor) {
        fullQuarter = Transform.rotateX(PI/divisor);
    }

    // Naming convention doesn't match because Cucumber was having trouble reading the title of this one
    @Then("half_quarter * p = point where x is square root of {int} over {int}, y is square root of {int} over {int}, and z is square root of {int} over {int}")
    public void half_quarterPPointWhereXIsSquareRootOfOverYIsSquareRootOfOverAndZIsSquareRootOfOver(int x1, int x2, int y1, int y2, int z1, int z2) {
        // need to check for 0 inputs or we get NaN from Math.sqrt()
        // This was done this way so the cucumber step where 0 can be input or a/b could be captured in one step
        float x = getQuotientOrZero((float)Math.sqrt(x1), x2);
        float y = getQuotientOrZero((float)Math.sqrt(y1), y2);
        float z = getQuotientOrZero((float)Math.sqrt(z1), z2);
        Tuple result = Tuple.createPoint(x, y, z);
        assertTrue(halfQuarter.multiply(point).equals(result));
    }

    @And("full_quarter * p = point at {int}, {int}, {int}")
    public void full_quarterPPoint(int x, int y, int z) {
        Tuple result = Tuple.createPoint(x, y, z);
        assertTrue(fullQuarter.multiply(point).equals(result));
    }

    @And("inv ← inverse\\(half_quarter)")
    public void invInverseHalf_quarter() {
        inverse = halfQuarter.getInverse();
    }

    @Then("inv * p = point where x is {int}, y is the square root of {int} over {int}, and z is the negated square root of {int} over {int}")
    public void invPPoint(int x, int y1, int y2, int z1, int z2) {
        float y = (float)Math.sqrt(y1)/y2;
        float z = -(float)Math.sqrt(z1)/z2;
        Tuple result = Tuple.createPoint(x, y, z);
        assertTrue(inverse.multiply(point).equals(result));
    }

    @And("half_quarter ← rotation about y of π divided by {int}")
    public void half_quarterRotation_y(int divisor) {
        halfQuarter = Transform.rotateY(PI/divisor);
    }

    @And("full_quarter ← rotation about y of π divided by {int}")
    public void full_quarterRotation_y(int divisor) {
        fullQuarter = Transform.rotateY(PI/divisor);
    }

    @And("half_quarter ← rotation about z of π divided by {int}")
    public void half_quarterRotation_z(int divisor) {
        halfQuarter = Transform.rotateZ(PI/divisor);
    }

    @And("full_quarter ← rotation about z of π divided by {int}")
    public void full_quarterRotation_z(int divisor) {
        fullQuarter = Transform.rotateZ(PI/divisor);
    }

    @Then("half_quarter * p = point where x is negated square root of {int} over {int}, y is square root of {int} over {int}, and z is square root of {int} over {int}")
    public void half_quarterPPointWhereXIsNegatedSquareRootOfOverYIsSquareRootOfOverAndZIsSquareRootOfOver(int x1, int x2, int y1, int y2, int z1, int z2) {
        // need to check for 0 inputs or we get NaN from Math.sqrt()
        // This was done this way so the cucumber step where 0 can be input or a/b could be captured in one step
        float x = getQuotientOrZero(-(float)Math.sqrt(x1), x2);
        float y = getQuotientOrZero((float)Math.sqrt(y1), y2);
        float z = getQuotientOrZero((float)Math.sqrt(z1), z2);
        Tuple result = Tuple.createPoint(x, y, z);
        assertTrue(halfQuarter.multiply(point).equals(result));
    }

    /**
     * Returns the quotient if valid division, otherwise if NaN or zero returns zero.
     */
    private float getQuotientOrZero(float dividend, float divisor){
        return divisor == 0 ? 0 : dividend / divisor;
    }

    @Given("transform ← shearing of {int}, {int}, {int}, {int}, {int}, {int}")
    public void transformShearing(int xToY, int xToZ, int yToX, int yToZ, int zToX, int zToY) {
        transform = Transform.shearing(xToY, xToZ, yToX, yToZ, zToX, zToY);
    }

    @And("A ← rotation about x of π divided by {int}")
    public void aRotation_x(int radians) {
        matrixA = Transform.rotateX(PI/radians);
    }

    @And("B ← scaling of {int}, {int}, {int}")
    public void bScaling(int x, int y, int z) {
        matrixB = Transform.scaling(x, y, z);
    }

    @And("C ← translation of {int}, {int}, {int}")
    public void cTranslation(int x, int y, int z) {
        matrixC = Transform.translation(x, y, z);
    }

    @When("p2 ← A * p")
    public void pAP() {
        point2 = matrixA.multiply(point);
    }

    @Then("p2 = point at {int}, {int}, {int}")
    public void pPoint2(int x, int y, int z) {
        assertTrue(point2.equals(Tuple.createPoint(x, y, z)));
    }

    @When("p3 ← B * p2")
    public void pBP() {
        point3 = matrixB.multiply(point2);
    }

    @Then("p3 = point at {int}, {int}, {int}")
    public void pPoint3(int x, int y, int z) {
        assertTrue(point3.equals(Tuple.createPoint(x, y, z)));
    }

    @When("p4 ← C * p3")
    public void pCP() {
        point4 = matrixC.multiply(point3);
    }

    @Then("p4 = point at {int}, {int}, {int}")
    public void pPoint4(int x, int y, int z) {
        assertTrue(point4.equals(Tuple.createPoint(x, y, z)));
    }

    @When("T ← C * B * A")
    public void tCBA() {
        matrixT = matrixC.multiply(matrixB).multiply(matrixA);
    }

    @Then("T * p = point at {int}, {int}, {int}")
    public void tPPoint(int x, int y, int z) {
        assertTrue(matrixT.multiply(point).equals(Tuple.createPoint(x, y, z)));
    }
}
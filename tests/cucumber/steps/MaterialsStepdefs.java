package cucumber.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import raytracer.Color;
import raytracer.Material;
import raytracer.PointLight;
import raytracer.Tuple;
import tools.FloatTool;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class MaterialsStepdefs {
    private Material mat;
    private Tuple matPosition;
    private Tuple eyev;
    private Tuple normalv;
    private PointLight light;
    private Color result;

    @Given("mat ← material")
    public void matMaterial() {
        mat = new Material();
    }

    @Then("mat.getColor = color of {int}, {int}, {int}")
    public void matGetColorColorOf(int r, int g, int b) {
        assertTrue(mat.getColor().equals(new Color(r, g, b)));
    }

    @And("mat.getAmbient = {float}")
    public void matGetAmbient(float ambient) {
        assertTrue(FloatTool.areFloatsEqual(ambient, mat.getAmbient()));
    }

    @And("mat.getDiffuse = {float}")
    public void matGetDiffuse(float diffuse) {
        assertTrue(FloatTool.areFloatsEqual(diffuse, mat.getDiffuse()));
    }

    @And("mat.getSpecular = {float}")
    public void matGetSpecular(float specular) {
        assertTrue(FloatTool.areFloatsEqual(specular, mat.getSpecular()));
    }

    @And("mat.getShininess = {float}")
    public void matGetShininess(float shininess) {
        assertTrue(FloatTool.areFloatsEqual(shininess, mat.getShininess()));
    }

    @And("matPosition ← point at {int}, {int}, {int}")
    public void matpositionPointAt(int x, int y, int z) {
        matPosition = Tuple.createPoint(x, y, z);
    }

    @Given("eyev ← vector of {int}, {int}, {int}")
    public void eyevVectorOf(int x, int y, int z) {
        eyev = Tuple.createVector(x, y, z);
    }

    @And("normalv ← vector of {int}, {int}, {int}")
    public void normalvVectorOf(int x, int y, int z) {
        normalv = Tuple.createVector(x, y, z);
    }

    @And("light ← PointLight at point {int}, {int}, {int} with color {int}, {int}, {int}")
    public void lightPointLightAtPointWithColor(int x, int y, int z, int r, int g, int b) {
        light = new PointLight(Tuple.createPoint(x, y, z), new Color(r, g, b));
    }

    @When("result ← mat.addLighting with light, matPosition, eyev, normalv")
    public void resultLightingWithMLightPositionEyevNormalv() {
        result = mat.addLighting(light, matPosition, eyev, normalv);
    }

    @Then("result = color of {float}, {float}, {float}")
    public void resultColorOf(float r, float g, float b) {
        assertTrue(result.equals(new Color(r, g, b)));
    }

    @Given("eyev ← vector of {int}, square root of {int} over {int}, negated square root of {int} over {int}")
    public void eyevVectorOfSquareRootOfOverNegatedSquareRootOfOver(int x, int y1, int y2, int z1, int z2) {
        float y = (float)Math.sqrt(y1) / y2;
        float z = -(float)Math.sqrt(z1) / z2;
        eyev = Tuple.createVector(x, y, z);
    }

    @Given("eyev ← vector of {int}, negated square root of {int} over {int}, negated square root of {int} over {int}")
    public void eyevVectorOfNegatedSquareRootOfOverNegatedSquareRootOfOver(int x, int y1, int y2, int z1, int z2) {
        float y = -(float)Math.sqrt(y1) / y2;
        float z = -(float)Math.sqrt(z1) / z2;
        eyev = Tuple.createVector(x, y, z);
    }
}

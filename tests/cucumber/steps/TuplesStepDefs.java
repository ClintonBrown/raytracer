package cucumber.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import raytracer.Tuple;

import static org.junit.jupiter.api.Assertions.assertTrue;

public class TuplesStepDefs {
    private Tuple vector;
    private Tuple normal;
    private Tuple reflection;

    @Given("vector ← vector of {int}, {int}, {int}")
    public void vectorVectorOf(int x, int y, int z) {
        vector = Tuple.createVector(x, y, z);
    }

    @And("normal ← vector of {int}, {int}, {int}")
    public void normalVectorOf(int x, int y, int z) {
        normal = Tuple.createVector(x, y, z);
    }

    @When("reflection ← vector.reflect of normal")
    public void reflectionVectorReflectOfNormal() {
        reflection = vector.reflect(normal);
    }

    @Then("reflection = vector of {int}, {int}, {int}")
    public void reflectionVectorOf(int x, int y, int z) {
        assertTrue(reflection.equals(Tuple.createVector(x, y, z)));
    }

    @And("normal ← vector of square root {int} over {int}, square root of {int} over {int}, {int}")
    public void normalVectorOfSquareRootOverSquareRootOfOver(int x1, int x2, int y1, int y2, int z) {
        float x = (float)Math.sqrt(x1) / x2;
        float y = (float)Math.sqrt(y1) / y2;
        normal = Tuple.createVector(x, y, z);
    }
}

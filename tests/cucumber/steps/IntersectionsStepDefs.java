package cucumber.steps;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import raytracer.Intersection;
import raytracer.Sphere;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class IntersectionsStepDefs {
    private Intersection i;
    private Intersection<Sphere> i1;
    private Intersection<Sphere> i2;
    private Intersection<Sphere> i3;
    private Intersection<Sphere> i4;
    private Sphere s;
    private ArrayList<Intersection> xs;

    @Given("s ← sphere to intersect")
    public void sSphereToIntersect() {
        s = new Sphere();
    }

    @When("i ← intersection at distance {float} with object s")
    public void iIntersectionAtDistanceWithObjectS(float distance) {
        i = new Intersection<>(distance, s);
    }

    @Then("i.getDistance = {float}")
    public void iGetDistance(float distance) {
        assertEquals(distance, i.getDistance());
    }

    @And("i.getObject = s")
    public void iGetObjectS() {
        assertEquals(s, i.getObject());
    }

    @And("i1 ← intersection at distance {int} with object s")
    public void i1IntersectionAtDistanceWithObjectS(int distance) {
        i1 = new Intersection<>(distance, s);
    }

    @And("i2 ← intersection at distance {int} with object s")
    public void i2IntersectionAtDistanceWithObjectS(int distance) {
        i2 = new Intersection<>(distance, s);
    }

    @When("xs ← collection of intersections i1, i2")
    public void xsCollectionOfIntersectionsII() {
        xs = new ArrayList<>();
        xs.add(i1);
        xs.add(i2);
    }

    @And("xs[{int}].distance = {int}")
    public void xsT(int i, int distance) {
        assertEquals(distance, xs.get(i).getDistance());
    }

    @Then("xs.size is {int}")
    public void xsSizeIs(int size) {
        assertEquals(size, xs.size());
    }

    @And("xs ← collection of intersections i2, i1")
    public void xsCollectionOfIntersectionsI2() {
        xs = new ArrayList<>();
        xs.add(i2);
        xs.add(i1);
    }

    @When("i ← hit from xs")
    public void iHitFromXs() {
        i = Intersection.hit(xs);
    }

    @Then("i = i1")
    public void iI() {
        assertEquals(i, i1);
    }

    @Then("i = i2")
    public void i2() {
        assertEquals(i, i2);
    }

    @Then("i is nothing")
    public void iIsNothing() {
        assertNull(i);
    }

    @And("i3 ← intersection at distance {int} with object s")
    public void i3IntersectionAtDistanceWithObjectS(int distance) {
        i3 = new Intersection<>(distance, s);
    }

    @And("i4 ← intersection at distance {int} with object s")
    public void i4IntersectionAtDistanceWithObjectS(int distance) {
        i4 = new Intersection<>(distance, s);
    }

    @And("xs ← collection of intersections i1, i2, i3, i4")
    public void xsCollectionOfIntersectionsIIII() {
        xs = new ArrayList<Intersection>();
        xs.add(i1);
        xs.add(i2);
        xs.add(i3);
        xs.add(i4);
    }

    @Then("i = i4")
    public void i4() {
        assertEquals(i, i4);
    }
}

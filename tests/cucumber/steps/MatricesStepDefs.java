package cucumber.steps;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import raytracer.Matrix;
import raytracer.Tuple;
import tools.IndexTool;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class MatricesStepDefs {
    private Matrix matrix;
    private Matrix matrixB;
    private Matrix matrixC;
    private Matrix identity;
    private Tuple aTuple;
    private Tuple bTuple;
    private List<List<Float>> dataTable;

    @Given("the following {int}x{int} matrix M:")
    public void the_following_x_matrix_M(int rows, int cols, DataTable dt){
        dataTable = dt.asLists(Float.class);
        matrix = new Matrix(dataTable);
    }

    @Then("M[{float}] = {int}")
    public void m(float cell, int expectedAnswer){
        IndexTool indexer = new IndexTool(cell);
        int x = indexer.getX();
        int y = indexer.getY();
        assertEquals(expectedAnswer, dataTable.get(y).get(x));
    }

    @Then("M[{float}] = {float}")
    public void m(float cell, float expectedAnswer){
        IndexTool indexer = new IndexTool(cell);
        int x = indexer.getX();
        int y = indexer.getY();
        assertEquals(expectedAnswer, dataTable.get(y).get(x));
    }

    @Given("the following matrix A:")
    public void theFollowingMatrixA(DataTable dt) {
        dataTable = dt.asLists(Float.class);
        matrix = new Matrix(dataTable);
    }

    @Given("the following matrix B:")
    public void theFollowingMatrixB(DataTable dt) {
        List<List<Float>> dataTableB = dt.asLists(Float.class);
        matrixB = new Matrix(dataTableB);
    }

    @Then("A = B")
    public void aB() {
        assertTrue(matrix.equals(matrixB));
    }

    @Then("A != B")
    public void aNotB() {
        assertFalse(matrix.equals(matrixB));
    }

    @Then("A * B is the following {int}x{int} matrix:")
    public void aBIsTheFollowingXMatrix(int rows, int cols, DataTable dt) {
        List<List<Float>> productTable = dt.asLists(Float.class);
        Matrix product = new Matrix(productTable);
        Matrix result = matrix.multiply(matrixB);
        assertTrue(result.equals(product));
    }

    @And("b ← tuple at {int}, {int}, {int}, {int}")
    public void bTuple(int x, int y, int z, int w) {
        bTuple = new Tuple((float)x, (float)y, (float)z, (float)w);
    }

    @Then("A * b = tuple at {int}, {int}, {int}, {int}")
    public void aBTuple(int x, int y, int z, int w) {
        Tuple product = new Tuple((float)x, (float)y, (float)z, (float)w);
        Tuple result = matrix.multiply(bTuple);
        assertTrue(result.equals(product));
    }

    @Then("A * identity_matrix = A")
    public void aIdentity_matrixA() {
        int height = matrix.getMatrix().size();
        int width = matrix.getMatrix().get(0).size();
        identity = Matrix.createIdentity(width, height);
        assertTrue(matrix.multiply(identity).equals(matrix));
    }

    @Given("a ← tuple at {int}, {int}, {int}, {int}")
    public void aTuple(int x, int y, int z, int w) {
        aTuple = new Tuple((float)x, (float)y, (float)z, (float)w);
    }

    @Then("identity_matrix * a = a")
    public void identity_matrixAA() {
        Tuple result = Matrix.createIdentity().multiply(aTuple);
        assertTrue(result.equals(aTuple));
    }

    @Then("transpose\\(A) is the following matrix:")
    public void transposeAIsTheFollowingMatrix(DataTable dt) {
        List<List<Float>> transposedTable = dt.asLists(Float.class);
        Matrix transposed = new Matrix(transposedTable);
        assertTrue(matrix.transpose().equals(transposed));
    }


    @Given("A ← transpose\\(identity_matrix)")
    public void aTransposeIdentity_matrix() {
        identity = Matrix.createIdentity();
        matrix = identity.transpose();
    }

    @Then("A = identity_matrix")
    public void aIdentity_matrix() {
        assertTrue(matrix.equals(identity));
    }

    @Given("the following {int}x{int} matrix A:")
    public void theFollowingXMatrixA(int rows, int cols, DataTable dt) {
        List<List<Float>> twoByTwoTable = dt.asLists(Float.class);
        matrix = new Matrix(twoByTwoTable);
    }

    @Then("determinant\\(A) = {int}")
    public void determinantA(int result) {
        assertEquals(result, matrix.getDeterminant());
    }

    @Then("submatrix\\(A, {int}, {int}) is the following {int}x{int} matrix:")
    public void submatrixAIsTheFollowingXMatrix(int rowToRemove, int colToRemove, int rows, int cols, DataTable dt) {
        List<List<Float>> subTable = dt.asLists(Float.class);
        Matrix submatrix = new Matrix(subTable);
        assertTrue(matrix.getSubmatrix(rowToRemove, colToRemove).equals(submatrix));
    }

    @And("B ← submatrix\\(A, {int}, {int})")
    public void bSubmatrixA(int rowToRemove, int colToRemove) {
        matrixB = matrix.getSubmatrix(rowToRemove, colToRemove);
    }

    @Then("determinant\\(B) = {int}")
    public void determinantB(int result) {
        assertEquals(result, matrixB.getDeterminant());
    }

    @And("minor\\(A, {int}, {int}) = {int}")
    public void minorA(int rowToRemove, int colToRemove, int result) {
        assertEquals(result, matrix.getMinor(rowToRemove, colToRemove));
    }

    @And("cofactor\\(A, {int}, {int}) = {int}")
    public void cofactorA(int rowToRemove, int colToRemove, int result) {
        assertEquals(result, matrix.getCofactor(rowToRemove, colToRemove));
    }

    @And("A is invertible")
    public void aIsInvertible() {
        assertTrue(matrix.isInvertible());
    }

    @And("A is not invertible")
    public void aIsNotInvertible() {
        assertFalse(matrix.isInvertible());
    }

    @And("B ← inverse\\(A)")
    public void bInverseA() {
        matrixB = matrix.getInverse();
    }

    @And("B[{float}] = {int} divided by {int}")
    public void b(float cell, int dividend, int divisor) {
        float expectedAnswer = (float)dividend / (float)divisor;
        IndexTool indexer = new IndexTool(cell);
        int x = indexer.getX();
        int y = indexer.getY();
        assertEquals(expectedAnswer, matrixB.getMatrix().get(y).get(x));
    }

    @And("B is the following {int}x{int} matrix:")
    public void bIsTheFollowingXMatrix(int rows, int cols, DataTable dt) {
        List<List<Float>> dataTableB = dt.asLists(Float.class);
        matrixB = new Matrix(dataTableB);
    }

    @Then("inverse\\(A) is the following {int}x{int} matrix:")
    public void inverseAIsTheFollowingXMatrix(int rows, int cols, DataTable dt) {
        List<List<Float>> inverseTable = dt.asLists(Float.class);
        Matrix inverse = new Matrix(inverseTable);
        assertTrue(matrix.getInverse().equals(inverse));
    }

    @And("C ← A * B")
    public void cAB() {
        matrixC = matrix.multiply(matrixB);
    }

    @Then("C * inverse\\(B) = A")
    public void cInverseBA() {
        assertTrue(matrixC.multiply(matrixB.getInverse()).equals(matrix));
    }
}
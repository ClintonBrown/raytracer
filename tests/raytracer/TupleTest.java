package raytracer;

import org.junit.jupiter.api.Test;
import tools.FloatTool;

import static org.junit.jupiter.api.Assertions.*;

class TupleTest {
    @Test
    void pointShouldBeCreated(){
        Tuple point = Tuple.createPoint(4.3f, -4.2f, 3.1f);

        // check that point was created correctly
        assertEquals(4.3f, point.getX());
        assertEquals(-4.2f, point.getY());
        assertEquals(3.1f, point.getZ());
        assertEquals(1.0f, point.getW());
        assertTrue(point.isPoint());
        assertFalse(point.isVector());
    }

    @Test
    void equalsPoint(){
        Tuple point1 = Tuple.createPoint(4.3f, -4.2f, 3.1f);
        Tuple point2 = Tuple.createPoint(4.2f, -4.1f, 3.0f);
        Tuple vector = Tuple.createVector(4.3f, -4.2f, 3.1f);

        // check that the above Tuples don't equal each other
        assertFalse(point1.equals(point2));
        assertFalse(point1.equals(vector));

        // set point2 equivalent point1 then test that they ARE equal
        point2 = Tuple.createPoint(4.3f, -4.2f, 3.1f);
        assertTrue(point1.equals(point2));
    }

    @Test
    void vectorShouldBeCreated(){
        Tuple vector = Tuple.createVector(4.3f, -4.2f, 3.1f);

        assertEquals(4.3f, vector.getX());
        assertEquals(-4.2f, vector.getY());
        assertEquals(3.1f, vector.getZ());
        assertEquals(0.0f, vector.getW());
        assertEquals(false, vector.isPoint());
        assertEquals(true, vector.isVector());
    }

    @Test
    void equalsVector(){
        Tuple vector1 = Tuple.createVector(4.3f, -4.2f, 3.1f);
        Tuple vector2 = Tuple.createVector(4.2f, -4.1f, 3.0f);
        Tuple point = Tuple.createPoint(4.3f, -4.2f, 3.1f);

        // check that the above Tuples don't equal each other
        assertFalse(vector1.equals(vector2));
        assertFalse(vector1.equals(point));

        // set vector2 equivalent vector1 then test that they ARE equal
        vector2 = Tuple.createVector(4.3f, -4.2f, 3.1f);
        assertTrue(vector1.equals(vector2));
    }

    @Test
    void addTuples(){
        Tuple point1 = Tuple.createPoint(3, -2, 5);
        Tuple point2 = Tuple.createPoint(-2, 3, 1);
        Tuple vector1 = Tuple.createVector(-2, 3, 1);
        Tuple vector2 = Tuple.createVector(3, -2, 5);
        Tuple pointSum = Tuple.createPoint(1, 1, 6);
        Tuple vectorSum = Tuple.createVector(1, 1, 6);
        Tuple tupleSum = new Tuple(1, 1, 6, 2);

        // point plus vector is valid = point
        assertTrue(point1.add(vector1).equals(pointSum));

        // vector plus vector is valid = vector
        assertTrue(vector1.add(vector2).equals(vectorSum));

        // point plus point, w = 2, returns Tuple
        assertTrue(point1.add(point2).equals(tupleSum));
    }

    @Test
    void subtractTuples(){
        Tuple point1 = Tuple.createPoint(3, 2, 1);
        Tuple point2 = Tuple.createPoint(5, 6, 7);
        Tuple vector1 = Tuple.createVector(3, 2, 1);
        Tuple vector2 = Tuple.createVector(5, 6, 7);
        Tuple pointDiff = Tuple.createPoint(-2, -4, -6);
        Tuple vectorDiff = Tuple.createVector(-2, -4, -6);
        Tuple tupleDiff = new Tuple(-2, -4, -6, -1);

        // point minus point = vector
        assertTrue(point1.subtract(point2).equals(vectorDiff));

        // point minus vector = point
        assertTrue(point1.subtract(vector2).equals(pointDiff));

        // vector minus vector = vector
        assertTrue(vector1.subtract(vector2).equals(vectorDiff));

        // vector minus point, w = -1, returns Tuple
        assertTrue(vector1.subtract(point2).equals(tupleDiff));
    }

    @Test
    void subtractTupleFromZeroVector(){
        Tuple zero = Tuple.createVector(0, 0, 0);
        Tuple vector = Tuple.createVector(1, -2, 3);
        Tuple difference = Tuple.createVector(-1, 2, -3);

        assertTrue(zero.subtract(vector).equals(difference));
    }

    @Test
    void negateTuple(){
        Tuple a = new Tuple(1, -2, 3, -4);
        Tuple negated = new Tuple(-1, 2, -3, 4);

        assertTrue(a.negate().equals(negated));
    }

    @Test
    void multiplyTupleByScalar(){
        Tuple multiplicand = new Tuple(1, -2, 3, -4);
        float multiplier = 3.5f;
        Tuple product = new Tuple(3.5f, -7, 10.5f, -14);

        assertTrue(multiplicand.scale(multiplier).equals(product));

        // Multiplying by fraction
        multiplicand = new Tuple(1, -2, 3, -4);
        multiplier = 0.5f;
        product = new Tuple(0.5f, -1, 1.5f, -2);

        assertTrue(multiplicand.scale(multiplier).equals(product));
    }

    @Test
    void divideTupleByScalar(){
        Tuple dividend = new Tuple(1, -2, 3, -4);
        float divisor = 2.0f;
        Tuple quotient = new Tuple(0.5f, -1, 1.5f, -2);

        assertTrue(dividend.divide(divisor).equals(quotient));
    }

    @Test
    void calculateMagnitude(){
        Tuple vector = Tuple.createVector(0, 1, 0);
        assertTrue(vector.getMagnitude() == 1);

        vector = Tuple.createVector(0, 0, 1);
        assertTrue(vector.getMagnitude() == 1);

        vector = Tuple.createVector(1, 2, 3);
        assertTrue(FloatTool.areFloatsEqual(vector.getMagnitude(), (float)Math.sqrt(14)));

        vector = Tuple.createVector(-1, -2, -3);
        assertTrue(FloatTool.areFloatsEqual(vector.getMagnitude(), (float)Math.sqrt(14)));
    }

    @Test
    void normalizeVectors(){
        Tuple vector = Tuple.createVector(4, 0, 0);
        Tuple normalized = Tuple.createVector(1, 0, 0);
        assertTrue(vector.normalize().equals(normalized));

        vector = Tuple.createVector(1, 2, 3);
        normalized = Tuple.createVector(1/(float)Math.sqrt(14), 2/(float)Math.sqrt(14), 3/(float)Math.sqrt(14));
        assertTrue(vector.normalize().equals(normalized));
    }

    @Test
    void calculateMagnitudeOfNormalizedVector(){
        Tuple vector = Tuple.createVector(1, 2, 3);
        Tuple normalized = vector.normalize();
        assertTrue(FloatTool.areFloatsEqual(normalized.getMagnitude(), 1));
    }

    @Test
    void calculateDotProduct(){
        Tuple vector1 = Tuple.createVector(1, 2, 3);
        Tuple vector2 = Tuple.createVector(2, 3, 4);
        assertTrue(FloatTool.areFloatsEqual(vector1.dotProduct(vector2), 20));
    }

    @Test
    void calculateCrossProduct(){
        Tuple vector1 = Tuple.createVector(1, 2, 3);
        Tuple vector2 = Tuple.createVector(2, 3, 4);
        Tuple product = Tuple.createVector(-1, 2, -1);
        assertTrue(vector1.crossProduct(vector2).equals(product));
        assertTrue(vector2.crossProduct(vector1).equals(product.negate()));
    }
}
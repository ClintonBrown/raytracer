package raytracer;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ColorTest {
    @Test
    void colorShouldBeCreated(){
        Color color = new Color(-0.5f, 0.4f, 1.7f);
        assertEquals(-0.5f, color.red());
        assertEquals(0.4f, color.green());
        assertEquals(1.7f, color.blue());
    }

    @Test
    void addColors(){
        Color color1 = new Color(0.9f, 0.6f, 0.75f);
        Color color2 = new Color(0.7f, 0.1f, 0.25f);
        Color colorSum = new Color(1.6f, 0.7f, 1.0f);

        assertTrue(color1.add(color2).equals(colorSum));
    }

    @Test
    void subtractColors(){
        Color color1 = new Color(0.9f, 0.6f, 0.75f);
        Color color2 = new Color(0.7f, 0.1f, 0.25f);
        Color colorSum = new Color(0.2f, 0.5f, 0.5f);

        assertTrue(color1.subtract(color2).equals(colorSum));
    }

    @Test
    void multiplyColorByScalar(){
        Color color1 = new Color(0.2f, 0.3f, 0.4f);
        Color colorFactor = new Color(0.4f, 0.6f, 0.8f);

        assertTrue(color1.scale(2).equals(colorFactor));
    }

    @Test
    void multiplyColorByColor(){
        Color color1 = new Color(1, 0.2f, 0.4f);
        Color color2 = new Color(0.9f, 1, 0.1f);
        Color colorFactor = new Color(0.9f, 0.2f, 0.04f);

        assertTrue(color1.multiply(color2).equals(colorFactor));
    }
}
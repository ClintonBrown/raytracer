package raytracer;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CanvasTest {
    @Test
    void createCanvas(){
        Canvas canvas = new Canvas(10, 20);
        assertEquals(10, canvas.getWidth());
        assertEquals(20, canvas.getHeight());

        Pixel[][] canvasPixelGrid = canvas.getPixelGrid();
        Color black = new Color(0, 0, 0);
        for (int r = 0; r < canvas.getHeight(); r++){
            for (int c = 0; c < canvas.getWidth(); c++){
                assertTrue(black.equals(canvasPixelGrid[r][c].getColor()));
            }
        }
    }

    @Test
    void writePixelsToCanvas(){
        Canvas canvas = new Canvas(10, 20);
        Color red = new Color(1, 0, 0);
        Pixel redPixel = new Pixel(red);
        canvas.setPixel(2, 3, redPixel);
        assertTrue(canvas.getPixel(2, 3).getColor().equals(red));
    }

    @Test
    void constructPPMHeader(){
        Canvas canvas = new Canvas(5, 3);
        String header = canvas.buildPPMHeader();
        String expect = "P3\n5 3\n255";
        assertEquals(expect, header);
    }

    @Test
    void constructPPMPixelData(){
        Canvas canvas = new Canvas(5, 3);
        Color c1 = new Color(1.5f, 0f, 0f);
        Color c2 = new Color(0f, 0.5f, 0f);
        Color c3 = new Color(-0.5f, 0f, 1f);
        canvas.setPixel(0, 0, new Pixel(c1));
        canvas.setPixel(2, 1, new Pixel(c2));
        canvas.setPixel(4, 2, new Pixel(c3));
        String pixelData = canvas.buildPPMPixelData();
        String expect = "255 0 0 0 0 0 0 0 0 0 0 0 0 0 0\n0 0 0 0 0 0 0 128 0 0 0 0 0 0 0\n0 0 0 0 0 0 0 0 0 0 0 0 0 0 255";
        assertEquals(expect, pixelData);
    }

    @Test
    void splittingLongLinesInPPM(){
        Canvas canvas = new Canvas(10, 2);
        for(int r = 0; r < canvas.getHeight(); r++){
            for (int c = 0; c < canvas.getWidth(); c++){
                canvas.setPixel(c, r, new Pixel(new Color(1f, 0.8f, 0.6f)));
            }
        }
        String pixelData = canvas.buildPPMPixelData();
        String expect = "255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204\n"
                      + "153 255 204 153 255 204 153 255 204 153 255 204 153\n"
                      + "255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204\n"
                      + "153 255 204 153 255 204 153 255 204 153 255 204 153";
        assertEquals(expect, pixelData);
    }

    @Test
    void checkPPMEndsWithNewLine(){
        Canvas canvas = new Canvas(5, 3);
        String ppm = canvas.createPPMString();
        String expect = "\n";
        assertEquals(expect, ppm.substring(ppm.length()-1));
    }
}